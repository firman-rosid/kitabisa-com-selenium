package kitabisa;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import ObjectRepository.CampaignPage;
import resource.Base;

public class CampaignPageTest extends Base {
	public WebDriver driver;
	public static Logger log = LogManager.getLogger(Base.class.getName());
	CampaignPage cp;

	@BeforeMethod
	public void BefMet() throws IOException  {

		driver = initializeDriver();
		driver.get(prop.getProperty("url"));

		cp = new CampaignPage(driver);

	}

	@AfterMethod
	public void AfMet() {
		driver.close();
		driver.quit();
	}
	
	@Test(groups = "kitabisa")
	public void Submit_donasi() throws InterruptedException {
		System.out.println("Case submit donasi");
		WebElement element = driver.findElement(By.xpath("//h3[contains(text(),'Doa-doa #OrangBaik')]"));
		Actions actions = new Actions(driver);
		actions.moveToElement(element);
		actions.perform();
		System.out.println("-----------------------------");
		cp.Select_index1().click();
		cp.Btn_donasi().click();
		cp.Input_donasi().sendKeys("10000");
		cp.Btn_lanjut_pembayaran().click();
		
		cp.Select_metode_pembayaran().click();
		cp.Input_fullname().sendKeys("Firmansyah");
		cp.Input_username().sendKeys("083811007710");
		cp.Btn_lanjut_pembayaran().click();
		
		Assert.assertEquals(true, cp.Summary_page().isDisplayed());
		cp.Arrow_back_penggalangan().click();
		cp.Arrow_back_campaign().click();
		Assert.assertEquals(true, cp.Home_page().isDisplayed());
		
	}

}
