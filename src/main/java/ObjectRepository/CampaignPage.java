package ObjectRepository;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CampaignPage {

	public WebDriver driver;

	public CampaignPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//img[@alt='Balita & Anak Sakit']/parent::div")
	WebElement select_cat;
	@FindBy(xpath = "(//div[@class=\"style__GridContainer-sc-__sc-1w9zwrq-0 oTgHF\"])[1]")
	WebElement select_index1;
	@FindBy(xpath = "//button[@id='campaign-detail_button_donasi-sekarang']")
	WebElement btn_donasi;
	
	@FindBy(xpath = "//input[@id='contribute_inputfield_amount-donation']")
	WebElement input_donasi;
	@FindBy(xpath = "//button[@id='contribute_button_lanjutkan-pembayaran']")
	WebElement btn_lanjut_pembayaran;
	@FindBy(xpath = "//span[contains(text(),'Transfer BCA')]")
	WebElement select_metode_pembayaran;
	
	@FindBy(xpath = "//input[@name='fullname']")
	WebElement input_fullname;
	@FindBy(xpath = "//input[@name='username']")
	WebElement input_username;
	
	@FindBy(xpath = "//span[contains(text(),'Instruksi Pembayaran')]")
	WebElement summary_page;
	@FindBy(xpath = "//img[@src='/assets/arrowDark.png']")
	WebElement arrow_back_penggalangan;
	@FindBy(xpath = "//div[@id='plain-header']")
	WebElement arrow_back_campaign;
	@FindBy(xpath = "//h3[contains(text(),'Ingin Menggalang Dana?')]")
	WebElement home_page;
	
	public WebElement Select_cat() {
		return select_cat;
	}
	public WebElement Select_index1() {
		return select_index1;
	}
	public WebElement Btn_donasi() {
		return btn_donasi;
	}
	public WebElement Input_donasi() {
		return input_donasi;
	}
	public WebElement Btn_lanjut_pembayaran() {
		return btn_lanjut_pembayaran;
	}
	public WebElement Select_metode_pembayaran() {
		return select_metode_pembayaran;
	}
	public WebElement Input_fullname() {
		return input_fullname;
	}
	public WebElement Input_username() {
		return input_username;
	}
	public WebElement Summary_page() {
		return summary_page;
	}
	public WebElement Arrow_back_penggalangan() {
		return arrow_back_penggalangan;
	}
	public WebElement Arrow_back_campaign() {
		return arrow_back_campaign;
	}
	public WebElement Home_page() {
		return home_page;
	}
	
}
